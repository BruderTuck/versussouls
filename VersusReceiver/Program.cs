﻿using CommandLine;
using System;
using VersusLib;
using VersusLib.abilities;
using VersusLib.common;
using VersusLib.networking;

namespace VersusReceiver
{
    class Program
    {
        class Options
        {
            [Option('i', "ip", HelpText = "IPAddress of the command socket")]
            public string ReceiverIP { get; set; }

            [Option('p', "port", HelpText = "Port for the commands socket", Default = Constants.DEFAULT_COMMAND_SOCKET_PORT)]
            public int CommandPort { get; set; }
        }

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);

            var options = new Options();
            var argsResult = Parser.Default.ParseArguments<Options>(args);

            NetworkConfig networkConfig = new NetworkConfig();

            argsResult.WithParsed(options =>
            {
                networkConfig = new NetworkConfig()
                {
                    CommandReceiverIp = options.ReceiverIP,
                    CommandPort = options.CommandPort
                };
            });

            var runtime = Runtime.GetInstance();

            runtime.Initialize(networkConfig);
            runtime.StartClient();
        }

        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            Console.WriteLine("Performing graceful exit...");
            var successfulShutdow = Runtime.GetInstance().Shutdown();
            if (successfulShutdow)
                Console.WriteLine("Graceful exit complete...");
            else
                Console.WriteLine("We crashed and burned while exiting...");
        }
    }
}
