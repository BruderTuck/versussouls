import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Constants } from 'src/app/entities/Constants';
import { User } from '../entities/User';
import { Ability } from '../entities/Ability';

@Injectable()
export class VersusService {
  public readonly constants = new Constants();
  private readonly urlBase = '';
  private readonly appBase = 'versus';
  private readonly apiBase = 'api';

  constructor(
    private http: HttpClient,
    public cookieService: CookieService,
    private router: Router
  ) {}

  public get<T>(url: string): Observable<T> {
    const fullPath = `${this.urlBase}/${this.apiBase}/${url}`;
    return this.http.get<T>(fullPath);
  }

  public getUserFromCookie(): Observable<User> {
    const userName = this.cookieService.get(
      this.constants.COOKIE_USER_NAME_FIELD_NAME
    );

    return this.get<User>(`getUser/${userName}`);
  }

  public userExists(userName: string): Observable<boolean> {
    return this.get<boolean>(`userExists/${userName}`);
  }

  public getUser(userName: string): Observable<User> {
    return this.get<User>(`getUser/${userName}`);
  }

  public getAllUser(): Observable<User[]> {
    return this.get<User[]>(`getAllUser`);
  }

  public registerUser(
    userName: string,
    ipAddress: string,
    faceId: number
  ): Observable<User> {
    return this.get<User>(`registerUser/${userName}/${ipAddress}/${faceId}`);
  }

  public removeUser(userName: string) {
    //return this.http.get<User>(`${this.apiBase}/removeUser/${userName}`);
    return this.get<User>(`removeUser/${userName}`);
  }

  public addAbility(
    targetUser: User
  ): Observable<Ability> {
    return this.get<Ability>(`addRandomAbility/${targetUser.name}`);
  }

  public sendAbility(
    sourceUserName: string,
    abilityId: number,
    targetUserName: string
  ): Observable<boolean> {
    //return this.http.get<boolean>(`${this.apiBase}/sendAbility/${sourceUserName}/${abilityName}/${targetUserName}`);
    return this.get<boolean>(`sendAbility/${sourceUserName}/${abilityId}/${targetUserName}`);
  }

  public updateFaceId(userName: string, faceId: number): Observable<boolean> {
    //return this.http.get<boolean>(`${this.apiBase}/updateFaceId/${userName}/${faceId}`);
    return this.get<boolean>(`updateFaceId/${userName}/${faceId}`);
  }

  public redirectTo(pageName: string) {
    const pathParts = [this.appBase, pageName];
    this.router.navigate(pathParts);
  }
}
