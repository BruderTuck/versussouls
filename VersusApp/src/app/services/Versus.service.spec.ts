/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { VersusService } from './Versus.service';

describe('Service: Versus', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VersusService]
    });
  });

  it('should ...', inject([VersusService], (service: VersusService) => {
    expect(service).toBeTruthy();
  }));
});
