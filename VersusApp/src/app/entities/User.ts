import { AbilityStack } from './AbilityStack';

export class User {
  constructor(public id: number, public name: string, public iPAddress: string, public currentAbilities: AbilityStack[], public faceId: number) {}
}
