import { Ability } from "./Ability";

export class AbilityStack {
  constructor(public ability: Ability, public amount: number) {}
}
