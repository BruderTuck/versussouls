import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Versus Souls';

  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer){
    this.matIconRegistry.addSvgIcon('face', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/img/face.svg'));
    this.matIconRegistry.addSvgIcon('fingerprint', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/img/fingerprint.svg'));
    this.matIconRegistry.addSvgIcon('add', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/img/add.svg'));
    this.matIconRegistry.addSvgIcon('settings', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/img/settings.svg'));
    this.matIconRegistry.addSvgIcon('pause', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/img/pause.svg'));
    this.matIconRegistry.addSvgIcon('play', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/img/play.svg'));

  }
}
