import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Ability } from 'src/app/entities/Ability';

export interface NewAbilityDialogData {
  ability: Ability;
}

@Component({
  selector: 'app-NewAbilityDialog',
  templateUrl: './NewAbilityDialog.component.html',
  styleUrls: ['./NewAbilityDialog.component.scss']
})
export class NewAbilityDialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<NewAbilityDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: NewAbilityDialogData) { }

  ngOnInit() {
  }

  onClick(){
    this.dialogRef.close();
  }
}
