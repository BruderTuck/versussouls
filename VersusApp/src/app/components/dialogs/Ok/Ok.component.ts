import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface OkDialogdata {
  text: string;
}

@Component({
  selector: 'app-Ok',
  templateUrl: './Ok.component.html',
  styleUrls: ['./Ok.component.css']
})
export class OkComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<OkComponent>, @Inject(MAT_DIALOG_DATA) public data: OkDialogdata) { }

  ngOnInit() {
  }

  onClick(){
    this.dialogRef.close();
  }
}
