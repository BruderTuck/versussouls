import { ThrowStmt } from '@angular/compiler';

export class ImagePool {
  public allImages: string[] = [
    'face01.jpg',
    'face02.jpg',
    'face03.jpg',
    'face04.jpg',
  ];

  constructor() {}

  public getImage(id: number) {
    return this.allImages[id];
  }

  public getRandomImageId() {
    return Math.floor(Math.random() * this.allImages.length);
  }
}
