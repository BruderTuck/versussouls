import { NewAbilityDialogComponent } from './dialogs/NewAbilityDialog/NewAbilityDialog.component';
import { OkComponent } from './dialogs/Ok/Ok.component';
import { RegisterComponent } from './../pages/Register/Register.component';
import { BrowserModule } from '@angular/platform-browser';
import { ArenaComponent } from './../pages/Arena/Arena.component';
import { LoginComponent } from './../pages/Login/Login.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VersusAppComponent } from '../pages/VersusApp/VersusApp.component';
import { MatSliderModule } from '@angular/material/slider';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatDialogModule} from '@angular/material/dialog';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {MatBadgeModule} from '@angular/material/badge';
import {MatRippleModule} from '@angular/material/core';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressBarModule} from '@angular/material/progress-bar';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatSliderModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatChipsModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule,
    MatDialogModule,
    MatGridListModule,
    MatMenuModule,
    MatListModule,
    MatBadgeModule,
    MatRippleModule,
    MatSnackBarModule,
    MatProgressBarModule,
    FlexLayoutModule,
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  declarations: [
    VersusAppComponent,
    LoginComponent,
    ArenaComponent,
    RegisterComponent,
    OkComponent,
    NewAbilityDialogComponent
  ]
})
export class ComponentsModule { }
