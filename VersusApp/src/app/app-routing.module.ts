import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArenaComponent } from './pages/Arena/Arena.component';
import { LoginComponent } from './pages/Login/Login.component';
import { RegisterComponent } from './pages/Register/Register.component';
import { VersusAppComponent } from './pages/VersusApp/VersusApp.component';

const routes: Routes = [
  { path: 'versus', component: VersusAppComponent, children:
  [
    { path: 'login', component: LoginComponent },
    { path: 'arena', component: ArenaComponent },
    { path: 'register', component: RegisterComponent }
  ]
},
  { path: '', redirectTo: 'versus', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
