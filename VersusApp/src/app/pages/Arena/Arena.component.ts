import { ImagePool } from './../../components/ImagePool';
import { NewAbilityDialogComponent } from './../../components/dialogs/NewAbilityDialog/NewAbilityDialog.component';
import { Ability } from 'src/app/entities/Ability';
import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/entities/User';
import { VersusService } from 'src/app/services/Versus.service';
import { MatDialog } from '@angular/material/dialog';
import { OkComponent } from 'src/app/components/dialogs/Ok/Ok.component';
import { MatRipple } from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { timer } from 'rxjs';

@Component({
  selector: 'app-Arena',
  templateUrl: './Arena.component.html',
  styleUrls: ['./Arena.component.css'],
})
export class ArenaComponent implements OnInit {
  imagePool: ImagePool = new ImagePool();
  otherPlayers: User[] = [];
  currentUser: User = new User(-1, 'defaultUser', '1337', [], 0);
  selectedPlayer: string = '';
  selectedAbility: number = -1;
  nextAbilityProgress: number = 1;
  secondsUntilNextAbility: number = 300;
  timerPaused: boolean = true;

  constructor(
    public service: VersusService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.reload();
    const source = timer(1000, 1000);
    const subscribe = source.subscribe(val => {
      this.IncrementAbilityTimer();
    });
    this.startTimer();
  }

  getNextAbilityProgress() {
    const inPercent = (this.nextAbilityProgress * 100) / this.secondsUntilNextAbility;
    return Math.floor(inPercent);
  }

  reload() {
    if (
      this.service.cookieService.check(
        this.service.constants.COOKIE_USER_NAME_FIELD_NAME
      )
    ) {
      const userNameFromCookie = this.service.cookieService.get(
        this.service.constants.COOKIE_USER_NAME_FIELD_NAME
      );

      this.service.userExists(userNameFromCookie).subscribe((result) => {
        if (result) {
          this.service.getAllUser().subscribe((result) => {
            const allUser = result;
            this.otherPlayers = allUser.filter(
              (usr) =>
                usr.name.toUpperCase() !== userNameFromCookie.toUpperCase()
            );
            this.currentUser = allUser.filter(
              (usr) =>
                usr.name.toUpperCase() === userNameFromCookie.toUpperCase()
            )[0];
          });
        } else {
          this.service.cookieService.delete(
            this.service.constants.COOKIE_USER_NAME_FIELD_NAME
          );
          this.service.redirectTo('login');
        }
      });
    } else {
      this.service.redirectTo('login');
    }
  }

  startTimer() {
    this.timerPaused = false;
  }

  stopTimer() {
    this.timerPaused = true;
  }

  setAbilityTimerThreshold(timeInSeconds: number){
    this.secondsUntilNextAbility = timeInSeconds;
  }

  IncrementAbilityTimer() {
    if(!this.timerPaused){
      this.nextAbilityProgress += 1;
      if(this.nextAbilityProgress === this.secondsUntilNextAbility){
        this.nextAbilityProgress = 1;
        this.onAddAbilityClick();
      }
    }
  }

  onAddAbilityClick() {
    this.service.addAbility(this.currentUser).subscribe(
        (result) => {
          if (result) {
            this.showAddAbilitySuccessDialog(result);
            this.reload();
          } else {
            this.showAbilityAddErrorDialog();
          }
        },
        (error) => this.showAbilityAddErrorDialog()
      );
  }

  onPlayerSelectClick(selectedPlayerName: string) {
    this.selectedPlayer = selectedPlayerName;
  }

  onAbilitySelectClick(selectedAbilityName: number) {
    this.selectedAbility = selectedAbilityName;

    if (
      this.selectedPlayer !== null &&
      this.selectedPlayer !== undefined &&
      this.selectedPlayer !== '' &&
      this.selectedAbility !== null &&
      this.selectedAbility !== undefined &&
      this.selectedAbility !== -1
    ) {
      this.service.sendAbility(this.currentUser.name, this.selectedAbility, this.selectedPlayer).subscribe(
          (result) => {
            if (result) {
              this.showConfirmationSnackbar();
              this.reload();
            } else {
              this.showSendAbilityAddErrorDialog();
              this.reload();
            }
          },
          (error) => this.showSendAbilityAddErrorDialog()
        );
    }
  }

  showConfirmationSnackbar() {
    this._snackBar.open('Nice', '', {
      duration: 2000,
    });
  }

  showSendAbilityAddErrorDialog() {
    this.dialog.open(OkComponent, {
      data: { text: 'Error while sending ability.' },
    });
  }

  showAddAbilitySuccessDialog(newAbility: Ability) {
    this.dialog.open(NewAbilityDialogComponent, {
      width: '350px',
      height: '250px',
      data: { ability: newAbility },
    });
  }

  showAbilityAddErrorDialog() {
    this.dialog.open(OkComponent, {
      data: { text: 'Error while adding ability.' },
    });
  }

  onLogoutClick() {
    this.service.cookieService.deleteAll();
    this.service.redirectTo('login');
  }

  onDeleteClick() {
    this.service.cookieService.deleteAll();
    this.service.removeUser(this.currentUser.name).subscribe(result => {
      this.service.redirectTo('login');
    });
  }

  onFaceSelectClick(faceId: number){
    this.service.updateFaceId(this.currentUser.name, faceId).subscribe(result => {
      if(result){
        this.showFaceChangeConfirmationSnackbar();
        this.reload();
      } else {
        this.showFaceIdChangeErrorDialog();
      }
    }, error => this.showFaceIdChangeErrorDialog())
  }


  showFaceChangeConfirmationSnackbar() {
    this._snackBar.open('Face changed!', 'Others have to refresh', {
      duration: 2000,
    });
  }

  showFaceIdChangeErrorDialog() {
    this.dialog.open(OkComponent, {
      data: { text: 'Error while changing face.' },
    });
  }
}
