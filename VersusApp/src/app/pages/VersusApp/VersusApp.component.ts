import { Component, OnInit, ViewChild } from '@angular/core';
import { RouterOutlet, RouterModule } from '@angular/router';
import { VersusService } from 'src/app/services/Versus.service';

@Component({
  selector: 'app-VersusApp',
  templateUrl: './VersusApp.component.html',
  styleUrls: ['./VersusApp.component.css'],
})
export class VersusAppComponent implements OnInit {
  constructor(private service: VersusService) {}
  outletActivated = false;

  ngOnInit() {
    if (
      this.service.cookieService.check(
        this.service.constants.COOKIE_USER_NAME_FIELD_NAME
      )
    ) {
      const userNameFromCookie = this.service.cookieService.get(
        this.service.constants.COOKIE_USER_NAME_FIELD_NAME
      );
      this.service.userExists(userNameFromCookie).subscribe((result) => {
        if (result) {
          this.service.redirectTo('arena');
        } else {
          this.service.cookieService.delete(
            this.service.constants.COOKIE_USER_NAME_FIELD_NAME
          );
        }
      });
    }
  }

  redirectTo(route: string){
    this.service.redirectTo(route);
  }

  onActivate(){
    this.outletActivated = true;
  }
}
