import { OkComponent } from './../../components/dialogs/Ok/Ok.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { VersusService } from 'src/app/services/Versus.service';

@Component({
  selector: 'app-Login',
  templateUrl: './Login.component.html',
  styleUrls: ['./Login.component.css'],
})
export class LoginComponent implements OnInit {
  selectedUserName:string = '';
  disableLoginButton: boolean = true;
  constructor(private service: VersusService, private dialog: MatDialog) {}

  ngOnInit() {
    if (
      this.service.cookieService.check(
        this.service.constants.COOKIE_USER_NAME_FIELD_NAME
      )
    ) {
      const userNameFromCookie = this.service.cookieService.get(
        this.service.constants.COOKIE_USER_NAME_FIELD_NAME
      );
      this.service.userExists(userNameFromCookie).subscribe((result) => {
        if (result) {
          this.service.redirectTo('arena');
        } else {
          this.service.cookieService.delete(
            this.service.constants.COOKIE_USER_NAME_FIELD_NAME
          );
        }
      }, error => {});
    }
  }


  onLoginClick(){
    this.service.getUser(this.selectedUserName).subscribe(result => {
      if(result != null){
        this.service.cookieService.set(this.service.constants.COOKIE_USER_NAME_FIELD_NAME, result.name);
        this.service.redirectTo('arena');
      } else {
        this.showUserDoesNotExistMessage();
      }
    }, error => this.showUserDoesNotExistMessage())
  }

  onUserNameChange(currentValue: string) {
    if(this.selectedUserName != null && this.selectedUserName != undefined && this.selectedUserName != ''){
      this.disableLoginButton = false;
    } else {
      this.disableLoginButton = true;
    }
  }

  showUserDoesNotExistMessage() {
    this.dialog.open(OkComponent, {
      data: {text: 'The given user does not exist!'}
    });
  }
}
