import { ImagePool } from './../../components/ImagePool';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { VersusService } from 'src/app/services/Versus.service';

@Component({
  selector: 'app-Register',
  templateUrl: './Register.component.html',
  styleUrls: ['./Register.component.css'],
})
export class RegisterComponent implements OnInit {
  selectedUserName: string = '';
  selectedIpAddress: string = '';
  disableRegisterButton: boolean = true;
  imagePool: ImagePool = new ImagePool();

  constructor(private service: VersusService, private dialog: MatDialog) {}

  ngOnInit() {}

  onRegisterClick() {
    this.service.cookieService.delete(
      this.service.constants.COOKIE_USER_NAME_FIELD_NAME
    );

    let randomFaceId = this.imagePool.getRandomImageId();

    this.service
      .registerUser(this.selectedUserName, this.selectedIpAddress, randomFaceId)
      .subscribe((result) => {
        if (result != null) {
          this.service.cookieService.set(
            this.service.constants.COOKIE_USER_NAME_FIELD_NAME,
            result.name
          );
          this.service.redirectTo('arena');
        }
      });
  }

  onValuesChange() {
    if (
      this.selectedUserName != null &&
      this.selectedUserName != undefined &&
      this.selectedUserName != '' &&
      this.selectedIpAddress != null &&
      this.selectedIpAddress != undefined &&
      this.selectedIpAddress != ''
    ) {
      this.disableRegisterButton = false;
    } else {
      this.disableRegisterButton = true;
    }
  }
}
