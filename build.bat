@echo off

echo Building VersusSouls...

rem clean up previous files
if exist .\release\NUL (
:deleteFolder
echo Removing old release folder...
rmdir /s /q release
echo Removing old release folder DONE
)

rem # # # # # # # # # # # # # # # # # # # # SERVER # # # # # # # # # # # # # # # # # # # # #  
echo Building server...

rem --------------------------------- DOTNET SERVER------------------------
rem build dotnet project 
cd VersusServer
call dotnet publish -o ./bin/Publish -c Release
cd ..

rem copy c# publish folder contents into release/server
if not exist .\release\server\NUL mkdir .\release\server
robocopy /e .\VersusServer\bin\Publish .\release\server

rem --------------------------------- ANGULAR -----------------------------
rem build angular project 
cd VersusApp
call ng build
cd ..
 
copy angular dist content into release/server/_content
if not exist .\release\server\_content\NUL mkdir .\release\server\_content
robocopy /e .\VersusApp\dist\VersusApp .\release\server\_content

echo Building server DONE

rem # # # # # # # # # # # # # # # # # # # # RECEIVER # # # # # # # # # # # # # # # # # # # # # 
echo Building receiver...

rem build dotnet project 
cd VersusReceiver
call dotnet publish -o ./bin/Publish -c Release
cd ..

if not exist .\release\server\NUL mkdir .\release\receiver
robocopy /e .\VersusReceiver\bin\Publish .\release\receiver

rem copy c# publish folder contents into release/server
copy .\resources\VersusSouls.exe .\release\receiver
copy .\resources\versus_commands.lua .\release\receiver

echo Building receiver DONE
echo Building VersusSouls DONE