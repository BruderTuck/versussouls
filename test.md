# Open Data Analysis
When creating a digital twin of a city, one of the key factors which governs the quality and depth of the digitalization is the access to (live) data of the state of the city. As as city is a system of many different components, such as public traffic, layout of the roads, traffic lights, crossroads etc, it is save to assume that some of the institutions that manage these components, provide access to informations regarding the  current state of the given component. In this document, we will examine the publicly available application programming interfaces (APIs) that provide information about the current state of public traffic in the city of Frankfurt(Main) as well as determine which data is useful and who provides said data. We will further provide a list of currently open questions regarding each API provider.

We are **not** looking for maps, traffic lights and layout of the roads, as we already get this information from the [open street map](https://www.openstreetmap.org/relation/62400) (OSM) in combination with [sumo](https://sumo.dlr.de/docs/index.html).

## TLDR
[RMVs API](https://opendata.rmv.de/site/start.html) has what we need (timetables, stations, bus/tramlines), but we need an authentication key, in order to use the API. We have requested access but are still waiting for a response.

## Provider
As of this date, we've identified the following institutions that provides publicly accessible APIs as viable:
* RMV
* OSM
* Deutsche Bahn (DB)

## RMV
### Accessible data
The [open data portal](https://opendata.rmv.de/site/start.html), provided by the RMV holds an array of interesting datapoints. Evaluating the API routes that are shown by the [RMV API page](https://www.rmv.de/hapi/) it is save to assume, that the desired arrival and departing times for each Tram- and Busline is accessible by using RMVs API, along with the GPS-Data and a description of each Station. We have been provided with an authentication key by RMV which has to be appended to every api request. There also exists a
static station file, which provides informations regarding all available stations, such as GPS Coordinates, Names and routes.

The Key is:
* b252703d-e4cb-43f2-98d8-2e8320daf786

We will provide example API Requests in the use cases section.
### Use cases
Using the information on public transport routes along with arrival and departure times, we can increase the precision of our city traffic simulation. Furthermore, using the API as a datasource, we can be quite certain, that the data upon which our simulation is build is up to date. Unfoutunatly, no live GPS data is provided, using the API.
The static infrastructure file, which was mentioned in the accessible data section, only finds limited use for us, as there are no time tables included, and for now, the relevant station data is already included in OSM.    

### Examples

**How to get all stations that have Frankfurt in the Name:**

https://www.rmv.de/hapi/location.name?accessId=b252703d-e4cb-43f2-98d8-2e8320daf786&input=Frankfurt&format=json

Components:
* API-Base URL          : https://www.rmv.de/hapi/
* Station search by name: location.name?
* Authentication Token  : accessId=b252703d-e4cb-43f2-98d8-2e8320daf786
* String to search with : input=Frankfurt
* Output format         : format=json

**How to get all stations in Frankfurt within a radius:**

Lets take the Hauptwache plaza as the center of Frankfurt. This means the center is at 50.113805, 8.679592 as lat and lon coordinates. The radius seems to be measured in meters but also is currently broken,
as increasing the readius to an arbitrary value, does not change the api results.

https://www.rmv.de/hapi/location.nearbystops?accessId=b252703d-e4cb-43f2-98d8-2e8320daf786&originCoordLat=50.113805&originCoordLong=8.679592&format=json

Components:
* API-Base URL            : https://www.rmv.de/hapi/
* Station search by coords: location.nearbystops?
* Authentication Token    : accessId=b252703d-e4cb-43f2-98d8-2e8320daf786
* Center coordinates      : originCoordLat=50.113805&originCoordLong=8.679592
* Radius                  : r=4000
* Output format           : format=json

**How to get the Departureboard for the Konstablerwache Station:**

Stations all have an extId, which is given by the stations search. The Konstablerwache Station has the extId 51094.

https://www.rmv.de/hapi/departureBoard?accessId=b252703d-e4cb-43f2-98d8-2e8320daf786&extId=51094&format=json

Components:
* API-Base URL            : https://www.rmv.de/hapi/
* Station search by coords: departureBoard?
* Authentication Token    : accessId=b252703d-e4cb-43f2-98d8-2e8320daf786
* ExtId of the station    : extId=51094
* Output format           : format=json

The result is a list of Journeys, along with the product that operates on the journey. A product is a public transport vehicle. So if a journey departs at Konstablerwache, the product could be U5, S9, Tram 12 and so on.


### Open questions
* Live data regarding delays?
* Live data regarding temporary route changes?
## OSM
### Accessible data
[Open street map](https://www.openstreetmap.org/relation/62400) is already incorporated into our project, as sumo can import OSM files and build a network based on that data.
OSM Provides routes for public transport, along with station data, such as if the station is wheelchair friendly. OSM does not provide a timetable for a route, only where a station is along with a chain on stations which are mapped to a public_transport tag. As this information is updated by many users, it is reasonable to assume, that OSM itself does not update this information automatically - i.e. by using the RMV API. 
OSM further provides a wide range of data, too much to display here. They do however provide a quite useful [documentation](https://wiki.openstreetmap.org/wiki/API_v0.6) with examples.

### Use cases
We can use the data in OSM to locate the routes of the public transport. This might be sufficient for a rudimentary test of the public transport lines. However to gain actual value from this information we need a timetable of (at least) the desired arrival and departure times, to feed our simulation. Having these datapoints however can make it easier to find routes in sumo and interact with the simulation.

### Open questions
--

## Deutsche Bahn (DB)
### Available data
The DB has a [developer platform](https://developer.deutschebahn.com/store/), which exposes multiple APIs. 
The available data includes

* Station data
* Timetables
* Live data for DB-Cargo
* Images of Trainstations
* Parking information
* etc

As we are primarily focusing the center and south of Frankfurt (Fra Uas to Sachsenhausen), we don't encounter a railway crossing of DB-Trains. This means that the DB-Cargo API is useless for our use case, same as with the parking and images APIs.  
We've created an account on the developer platform, which is free and does not contain personal informations.
Here are the credentials:
* User name = digitalTwinFraUas
* Password  = digitalTwinFraUas

### Use cases
Some of the DB-Trains can be of interest, mainly some of the S-Trains that travel between the [Konstablerwache](https://www.openstreetmap.org/node/5912274448), [Lokalbahnhof](https://www.openstreetmap.org/node/1506876412) and [Südbahnhof](https://www.openstreetmap.org/way/30280034), as they can possibly be used to reach the University faster, depending on the current state of traffic. As there is a small chance that S-Bahn data is not directly provided by RMVs API, the DB Fahrplan API might become relevant.

### Open questions
As of now, these APIs only work half of the time, as, for example, the Fahrplan API is not documented at all and only shows a small fraction of possible Stations (only the ones where Regional Trains and ICEs can stop as it seems). 
**We have contacted the DB Open data team** and waiting for a response, regarding this issue. In the late December weeks, it is optimistic to assume that we will get a response this year. As for now, we cannot use the DB-APIs to get the current Timetables for stations that are relevant to us.

