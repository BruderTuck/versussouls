-- CONSTANTS
------------------------------------------------------
ACCESS_MODE_READ = "r"
ACCESS_MODE_WRITE = "w"
SAVE_FILE_NAME = "versus_pipe_saves.txt"
COMMAND_PIPE_FILE_NAME = "versus_command_pipe.txt"
COMMANDS_FILE_NAME = "versus_commands.lua"

-- VARIABLES
------------------------------------------------------
list = getAddressList()
clock = os.clock
current_command_index = 0
save_file = nil
isRunning = false
path_to_commands_file = nil
path_to_save_file = nil
path_to_available_commands_file = nil
path_to_desired_folder = nil
available_commands = {}

-- HELPER FUNCTIONS
------------------------------------------------------
function file_exists(path_to_file)
    local f = io.open(path_to_file, "rb")
    if f then f:close() end
    return f ~= nil
end

function get_or_create_file(path_to_file, access_mode, optional_initial_value)
    optional_initial_value = optional_initial_value or "NO_DEFAULT_VALUE"
    if file_exists(path_to_file) then
        return io.open(path_to_file, access_mode)
    end

    local new_file = io.open(path_to_file, ACCESS_MODE_WRITE)
    if optional_initial_value ~= "NO_DEFAULT_VALUE" then
        new_file:write(optional_initial_value)
    end
    new_file:close()
    return get_or_create_file(path_to_file, access_mode)
end

function build_path_to_current_executable(file_name)
    return path_to_desired_folder .. "\\" .. file_name
end

function set_base_path(base_path)
    path_to_desired_folder = base_path
end

function sleep(n) -- seconds
    local t0 = clock()
    while clock() - t0 <= n do end
end

-- INIT
------------------------------------------------------

function initialize_command_pipe_file()
    path_to_commands_file = build_path_to_current_executable(COMMAND_PIPE_FILE_NAME)
    local command_pipe_file = get_or_create_file(path_to_commands_file, ACCESS_MODE_READ)
    command_pipe_file:close()
end

function initialize_save_file()
    path_to_save_file = build_path_to_current_executable(SAVE_FILE_NAME)
    local save_file = get_or_create_file(path_to_save_file, ACCESS_MODE_READ, 0)
    local file_contents = save_file:read()

    current_command_index = tonumber(file_contents)

    save_file:close()
end

function initialize_tables()
    list.getMemoryRecordByDescription("Open").Active = true
    list.getMemoryRecordByDescription("Enable Challenge Main").Active = true
    list.getMemoryRecordByDescription("Open - Table v1.2.4 - Game v1.15").Active = true
end

function initialize_commands()
    path_to_available_commands_file = build_path_to_current_executable(COMMANDS_FILE_NAME)
    local commands_file = get_or_create_file(path_to_save_file, ACCESS_MODE_READ, 0)
    commands_file:close()

    available_commands = dofile(path_to_available_commands_file)
end

function initialize()
    set_base_path(TrainerOrigin)
    initialize_save_file()
    initialize_command_pipe_file()
    initialize_commands()
    initialize_tables()
    isInitialized = true
end

-- SAVE FILE
------------------------------------------------------
function update_save_file()
    local save_file = get_or_create_file(path_to_save_file, ACCESS_MODE_WRITE)
    save_file:write(current_command_index)
    save_file:close()
end

-- COMMANDS
------------------------------------------------------
function update_commands()
    available_commands = dofile(path_to_available_commands_file)
end

-- COMMAND PROCESSING
------------------------------------------------------
function read_next_commands()
    local result = nil
    local n = 0
    local index = 0

    for line in io.lines(path_to_commands_file)
    do
        if n >= current_command_index then
            if result == nil then
                result = {}
            end
            result[index] = line
            index = index + 1
        end
        n=n+1
    end

    io.close()
    return result
end

function parse_command(command_string)
    local command = {}
    for name, value in string.gmatch(command_string, "(%w+)=(%w+)") do
        command[name] = value
    end
    return command
end

function process_command(command)
    local loaded_command = available_commands[command.name]
    if loaded_command ~= nil then
        loaded_command.execute()
    else
        print("command not found: " .. command.name)
    end
end

function process_next_commands()
    local status, raw_commands = pcall(read_next_commands)
    if status then
        if raw_commands ~= nil then
            for i = 0, #raw_commands
            do
                local command = parse_command(raw_commands[i])
                process_command(command)
            end
            current_command_index = current_command_index + #raw_commands+1
            update_save_file()
        end
    end
end

-- MAIN LOOP
------------------------------------------------------
function run()
    while(isRunning)
    do
        process_next_commands()
        sleep(1)
    end
end

function start()
    initialize()
    if isInitialized then
        isRunning = true
        run()
    end
end

start()