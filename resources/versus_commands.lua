-- HELPER
function activate_effect(effect_id)
    list.getMemoryRecordByDescription("Apply Effect").Active = true
    list.getMemoryRecordByDescription("effectTriggerStatus").Active = false
    list.getMemoryRecordByDescription("Effect ID to apply").Value = effect_id
    list.getMemoryRecordByDescription("effectTriggerStatus").Active = true
end

commands = {}

soulLevel = 0
currentSoulLevel = 0

strength = 0
currentStrength = 0

dex = 0
currentDex = 0

hp = 0
currentMaxHp = 0

stamina = 0
currentMaxStamina = 0

levelDiff = 0

--debugCommand
commands["__debug"] = {
    execute = function() 
        print("boop") 
    end
}

-- Player dies immedietly
commands["heartAttack"] = {
    execute = function() list.getMemoryRecordByDescription("HP").Value = 0 
    end
}

-- Sets player staina to 1
commands["exhaustStart"] = {
    execute = function() 
        stamina = list.getMemoryRecordByDescription("BaseMaxSP").Value
        list.getMemoryRecordByDescription("BaseMaxSP").Value = 1
    end
}

-- Sets player staina to 1
commands["exhaustEnd"] = {
    execute = function() 
        currentMaxStamina = list.getMemoryRecordByDescription("BaseMaxSP").Value
        list.getMemoryRecordByDescription("BaseMaxSP").Value = stamina + currentMaxStamina -1
    end
}

-- slightly increases head size
commands["einsteinStart"] = {
    execute = function()
        list.getMemoryRecordByDescription("Head Size").Value = list.getMemoryRecordByDescription("Head Size").Value + 0.1
    end
}

-- slightly decreases head size
commands["einsteinEnd"] = {
    execute = function()
        list.getMemoryRecordByDescription("Head Size").Value = list.getMemoryRecordByDescription("Head Size").Value - 0.1
    end
}

-- Start the top down camera
commands["topDownStart"] = {
    execute = function()
        list.getMemoryRecordByDescription("HUD").Value = 1 
        list.getMemoryRecordByID(499446).Active = false
        list.getMemoryRecordByID(499437).Active = false
        list.getMemoryRecordByDescription("First Person Cam").Active = false
        list.getMemoryRecordByDescription("Upside Down Souls").Active = false

        list.getMemoryRecordByDescription("Topdown cam").Active = true
    end
}

-- Ends the top down camera
commands["topDownEnd"] = {
    execute = function()
        list.getMemoryRecordByDescription("Topdown cam").Active = false
    end
}

-- Reduces HP to 1
commands["suddenDeathStart"] = {
    execute = function()
        activate_effect(3350)
        list.getMemoryRecordByDescription("NPC Stat Modifier").Active = true
        activate_effect(7092)
    end
}

-- Reduces HP to 1
commands["suddenDeathEnd"] = {
    execute = function()
    end
}

-- Force Bone back to last Bonfire (consumes no bone)
commands["adios"] = {
    execute = function() 
        activate_effect(580)
    end
}

-- Take double damage until using a bone/darksign or dying
commands["twiceTheFunStart"] = {
    execute = function() 
        activate_effect(12216)
        list.getMemoryRecordByDescription("NPC Stat Modifier").Active = true
        list.getMemoryRecordByDescription("Damage Multiplier").Value = 2
    end
}

commands["twiceTheFunEnd"] = {
    execute = function() 
        list.getMemoryRecordByDescription("Damage Multiplier").Value = 1
        list.getMemoryRecordByDescription("NPC Stat Modifier").Active = false
    end
}


-- Applies enough poison buildup to enable the poisoned status effect
commands["poison"] = {
    execute = function() 
        activate_effect(129011110)
        activate_effect(129011110)
        activate_effect(129011110)
        activate_effect(129011110)
        activate_effect(129011110)
        activate_effect(129011110)
        activate_effect(129011110)
        activate_effect(129011110)
        activate_effect(129011110)
        activate_effect(129011110)
    end
}




-- Applies enough toxic buildup to enable the toxiced status effect
commands["toxic"] = {
    execute = function() 
        activate_effect(3120)
        activate_effect(3120)
        activate_effect(3120)
        activate_effect(3120)
        activate_effect(3120)
        activate_effect(3120)
        activate_effect(3120)
        activate_effect(3120)
        activate_effect(3120)
    end
}

-- Applies maggots to the player which increase bleed buildup 
commands["maggots"] = {
    execute = function() 
        activate_effect(5100)
    end
}

-- Shows the text "Humanity Stolen" 
commands["humanityStolenBanner"] = {
    execute = function() 
        activate_effect(13)
    end
}

-- Applies the chameleon effect
commands["chameleon"] = {
    execute = function() 
        activate_effect(3870)
    end
}

-- Hides the player model and playes a cool effect at the beginning until bone is used or the player dies
commands["ninja"] = {
    execute = function() 
        activate_effect(4640)
    end
}

-- Increases Brightness and glow dramatically and reduces them back to normal slowly
commands["flashbang"] = {
    execute = function() 
        list.getMemoryRecordByDescription("Lighting Modifier").Active = true
        list.getMemoryRecordByDescription("Brightness").Value = 140
        list.getMemoryRecordByDescription("Glow").Value = 50

        for i = list.getMemoryRecordByDescription("Brightness").Value, 1, -0.03
        do
            list.getMemoryRecordByDescription("Brightness").Value = i
        end

        for i = list.getMemoryRecordByDescription("Glow").Value, 1, -0.1
        do
            list.getMemoryRecordByDescription("Glow").Value = i
        end
    end
}


commands["fullImmersionStart"] = {
    execute = function() 
        list.getMemoryRecordByDescription("Topdown cam").Active = false
        list.getMemoryRecordByDescription("Upside Down Souls").Active = false


        list.getMemoryRecordByDescription("HUD").Value = 0
        list.getMemoryRecordByDescription("First Person Cam").Active = true
        list.getMemoryRecordByID(499437).Active = true
        list.getMemoryRecordByID(499446).Active = true
    end
}

commands["fullImmersionEnd"] = {
    execute = function() 
        list.getMemoryRecordByDescription("HUD").Value = 1
        list.getMemoryRecordByID(499446).Active = false
        list.getMemoryRecordByID(499437).Active = false
        list.getMemoryRecordByDescription("First Person Cam").Active = false
    end
}

commands["upsideDownStart"] = {
    execute = function() 
        list.getMemoryRecordByDescription("Topdown cam").Active = false
        list.getMemoryRecordByDescription("HUD").Value = 1      
        list.getMemoryRecordByID(499446).Active = false
        list.getMemoryRecordByID(499437).Active = false
        list.getMemoryRecordByDescription("First Person Cam").Active = false

        list.getMemoryRecordByDescription("Upside Down Souls").Active = true
    end
}

commands["upsideDownEnd"] = {
    execute = function() 
        list.getMemoryRecordByDescription("Upside Down Souls").Active = false
    end
}

commands["whipItStart"] = {
    execute = function() 
        list.getMemoryRecordByDescription("Moveset Swap").Active = true
        list.getMemoryRecordByDescription("RightHandMoveset").Aktive = true
        list.getMemoryRecordByDescription("RightHandMoveset").Value = 43
    end
}

commands["whipItEnd"] = {
    execute = function() 
        list.getMemoryRecordByDescription("RightHandMoveset").Aktive = false
        list.getMemoryRecordByDescription("Moveset Swap").Active = false
    end
}

commands["yhormStart"] = {
    execute = function() 
        list.getMemoryRecordByDescription("Size Modifier").Active = true
        list.getMemoryRecordByDescription("Playersize").Value = 1.6
        list.getMemoryRecordByDescription("Movement Distance").Value = 0.4        
    end
}

commands["yhormEnd"] = {
    execute = function() 
        list.getMemoryRecordByDescription("Playersize").Value = 1
        list.getMemoryRecordByDescription("Movement Distance").Value = 1
    end
}

commands["speedyGonzalesStart"] = {
    execute = function() 
        list.getMemoryRecordByDescription("Size Modifier").Active = true
        list.getMemoryRecordByDescription("Playersize").Value = 0.5
        list.getMemoryRecordByDescription("Movement Distance").Value = 4        
    end
}

commands["speedyGonzalesEnd"] = {
    execute = function() 
        list.getMemoryRecordByDescription("Playersize").Value = 1
        list.getMemoryRecordByDescription("Movement Distance").Value = 1
        list.getMemoryRecordByDescription("Size Modifier").Active = false
    end
}

commands["speedSoulsStart"] = {
    execute = function() 
        list.getMemoryRecordByID(500269).Active = true
        list.getMemoryRecordByID(500271).Active = true
    end
}

commands["speedSoulsEnd"] = {
    execute = function() 
        
        list.getMemoryRecordByID(500269).Active = false
        list.getMemoryRecordByID(500271).Active = false
    end
}

commands["crackSoulsStart"] = {
    execute = function() 
        list.getMemoryRecordByDescription("Speed Modifier").Active = true
        list.getMemoryRecordByDescription("Speed Randomizer").Active = false
        list.getMemoryRecordByDescription("Manual Control").Active = true
        list.getMemoryRecordByID(500278).Value = 1.75

    end
}

commands["crackSoulsEnd"] = {
    execute = function() 
        list.getMemoryRecordByID(500278).Value = 1
        list.getMemoryRecordByDescription("Manual Control").Active = false
        list.getMemoryRecordByDescription("Speed Modifier").Active = false
    end
}

commands["veryDarkSoulsStart"] = {
    execute = function() 
        list.getMemoryRecordByDescription("Lighting Modifier").Active = true
        list.getMemoryRecordByDescription("Darkest Souls Option 2").Active = true
    end
}

commands["veryDarkSoulsEnd"] = {
    execute = function() 
        list.getMemoryRecordByDescription("Darkest Souls Option 2").Active = false
        list.getMemoryRecordByDescription("Lighting Modifier").Active = false
    end
}

commands["doubleTroubleStart"] = {
    execute = function() 
        list.getMemoryRecordByDescription("NPC Stat Modifier").Active = true
        list.getMemoryRecordByDescription("HP Multiplier").Value = 2
    end
}

commands["doubleTroubleEnd"] = {
    execute = function() 
        list.getMemoryRecordByDescription("HP Multiplier").Value = 1
        list.getMemoryRecordByDescription("NPC Stat Modifier").Active = false
    end
}
commands["fatRollStart"] = {
    execute = function() 
        list.getMemoryRecordByID(80515).Active = true
        list.getMemoryRecordByID(80516).Value = 3
    end
}

commands["fatRollEnd"] = {
    execute = function() 
        list.getMemoryRecordByID(80515).Active = false
    end
}

commands["fourteenIsFine"] = {
    execute = function() 
        if tonumber(list.getMemoryRecordByDescription("Vigor").Value) > 14 then
            levelDiff = tonumber(list.getMemoryRecordByDescription("Vigor").Value) - 14
            list.getMemoryRecordByDescription("Vigor").Value = 14
            list.getMemoryRecordByDescription("Soul Level").Value = tonumber(list.getMemoryRecordByDescription("Soul Level").Value) - levelDiff
          end
    end
}

-- Reduces strength by 5
commands["smallBiceps"] = {
    execute = function() 
        list.getMemoryRecordByID(1337036791).Value = list.getMemoryRecordByID(1337036791).Value - 5
        list.getMemoryRecordByDescription("Soul Level").Value = tonumber(list.getMemoryRecordByDescription("Soul Level").Value) - 5

    end
}

-- Reduces strength by 5
commands["twoLeftHands"] = {
    execute = function() 
        list.getMemoryRecordByID(1337036790).Value = list.getMemoryRecordByID(1337036790).Value - 5
        list.getMemoryRecordByDescription("Soul Level").Value = tonumber(list.getMemoryRecordByDescription("Soul Level").Value) - 5
    end
}

-- re imports this command file in case there are changes
commands["updateCommands"] = {execute = function() update_commands() end}

-- stops the trainer loop
commands["Q"] = {execute = function() isRunning = false end}
return commands
