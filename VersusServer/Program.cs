﻿using CommandLine;
using System;
using VersusLib;
using VersusLib.common;
using VersusLib.networking;

namespace VersusServer
{
    class Program
    {
        class Options
        {
            [Option('i', "ip", HelpText = "IPAddress of the API-Server")]
            public string APIServerIP { get; set; }

            [Option('p', "api-port", HelpText = "Port of the API-Server", Default = Constants.DEFAULT_API_SERVER_PORT)]
            public int APIServerPort { get; set; }

            [Option('c', "command-port", HelpText = "Port for the commands socket", Default = Constants.DEFAULT_COMMAND_SOCKET_PORT)]
            public int CommandPort { get; set; }
        }

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);

            var options = new Options();
            var argsResult = Parser.Default.ParseArguments<Options>(args);

            NetworkConfig networkConfig = new NetworkConfig();

            argsResult.WithParsed(options => {
                networkConfig = new NetworkConfig()
                {
                    APIServerIp = options.APIServerIP,
                    APIServerPort = options.APIServerPort,
                    CommandPort = options.CommandPort
                };
            });

            var runtime = Runtime.GetInstance();

            runtime.Initialize(networkConfig);
            runtime.StartAPIServer();
        }

        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            Console.WriteLine("Performing graceful exit...");
            Console.WriteLine("Graceful exit complete");
        }
    }
}



