﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VersusLib.networking
{
    public class NetworkConfig
    {
        public string APIServerIp { get; set; }
        public int APIServerPort { get; set; }
        public string CommandReceiverIp { get; set; }
        public int CommandPort { get; set; }

        public NetworkConfig()
        {
            this.APIServerPort = -1;
            this.CommandPort = -1;
        }
    }
}
