﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using VersusLib.common;
using VersusLib.networking;

namespace VersusLib.networking
{
    public class NetworkHandler
    {
        private static NetworkHandler _networkHandler;
        private NetworkConfig _config;
        private static string ReceiverIp = null;
        private static int CommandPort = -1;

        private NetworkHandler()
        {

        }

        public static NetworkHandler GetInstance()
        {
            if (_networkHandler is null)
                _networkHandler = new NetworkHandler();
            return _networkHandler;
        }

        public void Initialize(NetworkConfig networkConfig)
        {
            _config = networkConfig;
            ReceiverIp = networkConfig.CommandReceiverIp is null ? GetLocalIp() : networkConfig.CommandReceiverIp;
            CommandPort = networkConfig.CommandPort < 0 ? 8082 : networkConfig.CommandPort;
        }

        public static void SendMessage(string ipAddress, string message)
        {
            try
            {
                IPAddress ipAddr = Dns.GetHostAddresses(ipAddress)[0];
                IPEndPoint localEndPoint = new IPEndPoint(ipAddr, CommandPort);
                Socket sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                sender.Connect(localEndPoint);
                byte[] messageSent = Encoding.ASCII.GetBytes(message);
                int byteSent = sender.Send(messageSent);
                sender.Shutdown(SocketShutdown.Both);
                sender.Close();

            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
            }
            catch (SocketException se)
            {
                Console.WriteLine("SocketException : {0}", se.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static void ExecuteReceiver()
        {
            IPAddress ipAddr = Dns.GetHostAddresses(ReceiverIp)[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddr, CommandPort);
            Socket listener = new Socket(ipAddr.AddressFamily,
                         SocketType.Stream, ProtocolType.Tcp);

            Console.WriteLine($"Listening on: {ReceiverIp}:{CommandPort}");

            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);

                while (true)
                {
                    Socket clientSocket = listener.Accept();

                    byte[] bytes = new Byte[1024];
                    string data = null;

                    int numByte = clientSocket.Receive(bytes);

                    data = Encoding.ASCII.GetString(bytes, 0, numByte);

                    Console.WriteLine($"Received Text: {data}");

                    var command = $"name={data}";

                    FileHandler.AppendCommandPipeFile(command);
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("Error while running receiver: " + e.ToString());
            }
        }

        public static string GetLocalIp()
        {
            string localIP;
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", 65530);
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                localIP = endPoint.Address.ToString();
            }

            return localIP;
        }
    }
}
