﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using VersusLib.abilities.implementations;
using VersusLib.interfaces;
using VersusLib.user;

namespace VersusLib.networking
{
    public class Command
    {
        const int MILLIS_IN_A_SECOND = 1000;
        public IAbility Ability{ get; set; }
        public User Source { get; set; }
        public User Target { get; set; }
        public bool IsFinished { get; set; }
        public bool HasStarted { get; set; }

        public Command(IAbility ability, User source, User target)
        {
            Ability = ability;
            Source = source;
            Target = target;
            IsFinished = false;
            HasStarted = false;
        }

        public void Execute()
        {
            Console.WriteLine($"{Source.Name} has used {Ability.Name} on {Target.Name}");
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                HasStarted = true;
                if      (this.Ability is SingleAbility singleAbility) this.ExecuteSingleAbility(singleAbility, Source, Target);
                else if (this.Ability is TimedAbility timedAbility) this.ExecuteTimedAbility(timedAbility, Source, Target);
                else if (this.Ability is IterativeAbility iterativeAbility) this.ExecuteIterativeAbility(iterativeAbility, Source, Target);
                else if (this.Ability is TimedIterativeAbility timedIterativeAbility) this.ExecuteTimedIterativeAbility(timedIterativeAbility, Source, Target);
                IsFinished = true;
            }).Start();
        }

        private void ExecuteSingleAbility(SingleAbility ability, User source, User target)
        {
            NetworkHandler.SendMessage(target.IPAddress, $"{ability.Name}");
        }
        private void ExecuteTimedAbility(TimedAbility timedAbility, User source, User target)
        {
            var durationInMillis = timedAbility.DurationInSeconds * MILLIS_IN_A_SECOND;

            NetworkHandler.SendMessage(target.IPAddress, $"{timedAbility.Name}Start");
            Thread.Sleep(durationInMillis);
            NetworkHandler.SendMessage(target.IPAddress, $"{timedAbility.Name}End");
        }
        private void ExecuteIterativeAbility(IterativeAbility iterativeAbility, User source, User target)
        {
            var delayBetweenIterationsInMillis = iterativeAbility.DelayBetweenIterationsInSeconds * MILLIS_IN_A_SECOND;

            for (int i = 0; i < iterativeAbility.Iterations; i++)
            {
                NetworkHandler.SendMessage(target.IPAddress, $"{iterativeAbility.Name}");
                Thread.Sleep(delayBetweenIterationsInMillis);
            }
        }
        private void ExecuteTimedIterativeAbility(TimedIterativeAbility timedIterativeAbility, User source, User target)
        {
            var delayBetweenIterationsInMillis = timedIterativeAbility.DelayBetweenIterationsInSeconds * MILLIS_IN_A_SECOND;

            for (int i = 0; i < timedIterativeAbility.Iterations; i++)
            {
                NetworkHandler.SendMessage(target.IPAddress, $"{timedIterativeAbility.Name}Start");
                Thread.Sleep(delayBetweenIterationsInMillis);
            }

            for (int i = 0; i < timedIterativeAbility.Iterations; i++)
            {
                NetworkHandler.SendMessage(target.IPAddress, $"{timedIterativeAbility.Name}End");
                Thread.Sleep(delayBetweenIterationsInMillis);
            }
        }
    }
}
