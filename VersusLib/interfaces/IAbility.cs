﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VersusLib.interfaces
{
    public interface IAbility
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Single { get; set; }
        public string Description { get; set; }

        IAbility CreateCopy();
    }
}
