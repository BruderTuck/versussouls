﻿using System;
using System.Collections.Generic;
using System.Text;
using VersusLib.user;

namespace VersusLib.interfaces
{
    public interface IUserStore
    {
        bool Initialize();
        void AddUser(User user);
        IEnumerable<User> GetAllUser();
        User GetUserByName(string userName);
        bool RemoveUser(string userName);
        void UpdateSaveFile();
    }
}
