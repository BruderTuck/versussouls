﻿using System;
using System.Collections.Generic;
using System.Text;
using VersusLib.abilities;

namespace VersusLib.interfaces
{
    public interface IAbilityProvider
    {
        IEnumerable<AbilityInfo> GetAllAbilities();
    }
}
