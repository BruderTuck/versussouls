﻿using System;
using System.Collections.Generic;
using System.Text;
using VersusLib.abilities;

namespace VersusLib.interfaces
{
    public interface IAbilityFactory
    {
        IAbilityProvider AbilityProvider { get; set; }
        AbilityInfo GetAbilityInfo(int abilityId);
        AbilityInfo GetWeightedRandomAbilityInfo();
        IEnumerable<AbilityInfo> GetAllAbilityInfos();
    }
}
