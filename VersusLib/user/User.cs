﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VersusLib.abilities;
using VersusLib.interfaces;

namespace VersusLib.user
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IPAddress { get; set; }
        public List<AbilityStack> CurrentAbilities { get; set; }
        public int FaceId { get; set; }

        public User()
        {

        }
        public User(string name, string ipAddress, int faceId)
        {
            this.Id = -1;
            this.Name = name;
            this.IPAddress = ipAddress;
            this.CurrentAbilities = new List<AbilityStack>();
            this.FaceId = faceId;
        }

        public User(string name, string ipAddress, int faceId, List<AbilityStack> abilities)
        {
            this.Id = -1;
            this.Name = name;
            this.IPAddress = ipAddress;
            this.CurrentAbilities = abilities;
            this.FaceId = faceId;
        }

        public void AddAbility(IAbility ability)
        {
            lock (CurrentAbilities)
            {
                if (this.HasAbility(ability.Id))
                    this.GetAbility(ability.Id).Amount++;
                else
                    this.CurrentAbilities.Add(new AbilityStack
                    {
                        Ability = ability,
                        Amount = 1
                    });
            }
        }

        public IAbility PopAbility(int abilityId)
        {
            lock (CurrentAbilities)
            {
                IAbility result = null;

                if (this.HasAbility(abilityId))
                    if (this.GetAbility(abilityId).Amount > 0)
                    {
                        this.GetAbility(abilityId).Amount--;
                        result = this.GetAbility(abilityId).Ability.CreateCopy();

                        if (this.GetAbility(abilityId).Amount == 0)
                            this.RemoveAbility(abilityId);
                    }
                    else
                        this.RemoveAbility(abilityId);
                
                return result;
            }
        }

        public void RemoveAbility(int abilityId)
        {
            this.CurrentAbilities.RemoveAll(ability => ability.Ability.Id == abilityId);
        }

        public bool HasAbility(int abilityId)
        {
            return this.CurrentAbilities.Exists(ability => ability.Ability.Id == abilityId);
        }

        public AbilityStack GetAbility(int abilityId)
        {
            return this.CurrentAbilities.Single(ability => ability.Ability.Id == abilityId);
        }

        public bool Equals(User otherUser)
        {
            return this.HasName(otherUser.Name);
        }

        public bool HasName(string name)
        {
            return this.Name.ToUpper().Equals(name.ToUpper());
        }
    }
}
