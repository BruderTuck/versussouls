﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using VersusLib.common;
using VersusLib.interfaces;

namespace VersusLib.user
{
    public class FileUserStore : IUserStore
    {
        private List<User> _registeredUsers;

        public FileUserStore() { }
        
        public bool Initialize() => this.LoadUserFromSaveFile();
        
        private bool LoadUserFromSaveFile()
        {
            var usersAsJson = FileHandler.ReadUserSaveFile();
            if (usersAsJson != null)
                _registeredUsers = JsonSerializer.Deserialize<List<User>>(usersAsJson);
            else
                _registeredUsers = new List<User>();

            return true;
        }
        public void AddUser(User newUser)
        {
            lock (_registeredUsers)
            {
                if (_registeredUsers.Exists(user => user.Equals(newUser)))
                    this.RemoveUser(newUser.Name);

                newUser.Id = this.GetNextUserId();
                _registeredUsers.Add(newUser);
                this.UpdateSaveFile();
            }
        }
        public IEnumerable<User> GetAllUser() => _registeredUsers;
        
        public User GetUserByName(string userName) => _registeredUsers.FirstOrDefault(user => user.Name.ToUpper().Equals(userName.ToUpper()));

        public bool RemoveUser(string userName)
        {
            bool result = _registeredUsers.RemoveAll(user => user.HasName(userName)) > 0;
            this.UpdateSaveFile();
            
            return result;
        }
        public void UpdateSaveFile()
        {
            string usersAsJson = JsonSerializer.Serialize(_registeredUsers);
            FileHandler.WriteUserSave(usersAsJson);
        }

        private int GetNextUserId() => _registeredUsers.Count;
    }
}
