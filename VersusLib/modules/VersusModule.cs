﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VersusLib.networking;
using VersusLib.user;

namespace VersusLib.modules
{
    public class VersusModule : NancyModule
    {
        private readonly Runtime _runtime = Runtime.GetInstance();

        public VersusModule()
        {
            Get("/api/registerUser/{userName}/{ipAddress}/{faceId}", parameters => RegisterNewUser(parameters));
            Get("/api/getAllUser", parameters => GetAllUser());
            Get("/api/userExists/{userName}", parameters => GetUserExists(parameters));
            Get("/api/getUser/{userName}", parameters => GetUser(parameters));
            Get("/api/removeUser/{userName}", parameters => RemoveUser(parameters));
            Get("/api/sendAbility/{source}/{abilityId}/{target}", parameters => SendAbility(parameters));
            Get("/api/addRandomAbility/{userName}", parameters => AddRandomAbility(parameters));
            Get("/api/updateFaceId/{userName}/{faceId}", parameters => UpdateFaceID(parameters));
        }

        private dynamic GetUserExists(dynamic parameters)
        {
            string userName = parameters.userName;

            if (_runtime.GetUserStore().GetUserByName(userName) != null)
                return Response.AsJson(true).WithHeader("Access-Control-Allow-Origin", "*");

            return Response.AsJson(false).WithHeader("Access-Control-Allow-Origin", "*");
        }

        private dynamic AddRandomAbility(dynamic parameters)
        {
            string userName = parameters.userName;

            var newAbilityInfo = _runtime.GetAbilityFactory().GetWeightedRandomAbilityInfo();
            var user = _runtime.GetUserStore().GetUserByName(userName);
            
            if (user != null)
            {
                for (int i = 0; i< newAbilityInfo.Quantity; i++)
                    user.AddAbility(newAbilityInfo.Ability);
            
                _runtime.GetUserStore().UpdateSaveFile();
                return Response.AsJson(newAbilityInfo.Ability).WithHeader("Access-Control-Allow-Origin", "*");
            }
                
            return Response.AsJson(false).WithHeader("Access-Control-Allow-Origin", "*");
        }

        private dynamic GetAllUser()
        {
            var allUsers = _runtime.GetUserStore().GetAllUser();
            return Response.AsJson(allUsers).WithHeader("Access-Control-Allow-Origin", "*");
        }

        private dynamic GetUser(dynamic parameters)
        {
            string userName = parameters.userName;
            return Response.AsJson(_runtime.GetUserStore().GetUserByName(userName)).WithHeader("Access-Control-Allow-Origin", "*");
        }

        private dynamic RemoveUser(dynamic parameters)
        {
            string userName = parameters.userName;
            var result = this._runtime.GetUserStore().RemoveUser(userName);
            return Response.AsJson(result).WithHeader("Access-Control-Allow-Origin", "*");
        }

        private dynamic RegisterNewUser(dynamic parameters)
        {
            string userName = parameters.userName;
            string ipAddress = parameters.ipAddress;
            int faceId = parameters.faceId;

            var newUser = new User(userName, ipAddress, faceId);

            _runtime.GetUserStore().AddUser(newUser);
            Console.WriteLine($"Added new user <{newUser.Name}, {newUser.IPAddress}>");

            return Response.AsJson(_runtime.GetUserStore().GetUserByName(userName)).WithHeader("Access-Control-Allow-Origin", "*");
        }

        private dynamic SendAbility(dynamic parameters)
        {
            string sourceUserName = parameters.source;
            int abilityId = parameters.abilityId;
            string targetUserName = parameters.target;

            var sourceUser = _runtime.GetUserStore().GetUserByName(sourceUserName);
            var targetUser = _runtime.GetUserStore().GetUserByName(targetUserName);

            if (sourceUser != null && targetUser != null)
            {
                var ability = sourceUser.PopAbility(abilityId);
                if (ability != null)
                {
                    var command = new Command(ability, sourceUser, targetUser);
                    command.Execute();
                    _runtime.GetUserStore().UpdateSaveFile();

                    return Response.AsJson(true).WithHeader("Access-Control-Allow-Origin", "*");
                }
            }
            
            return Response.AsJson(false).WithHeader("Access-Control-Allow-Origin", "*");
        }

        private dynamic UpdateFaceID(dynamic parameters)
        {
            string userName = parameters.userName;
            int newFaceId = parameters.faceId;

            var user = _runtime.GetUserStore().GetUserByName(userName);
            if(user != null)
            {
                user.FaceId = newFaceId;
                _runtime.GetUserStore().UpdateSaveFile();

                return Response.AsJson(true).WithHeader("Access-Control-Allow-Origin", "*");
            }

            return Response.AsJson(false).WithHeader("Access-Control-Allow-Origin", "*");
        }
    }
}
