﻿using System;
using System.Collections.Generic;
using System.Text;
using VersusLib.abilities.implementations;
using VersusLib.common;
using VersusLib.interfaces;

namespace VersusLib.abilities
{
    public class HardcodedAbilityProvider : IAbilityProvider
    {
        // These values are in seconds
        const int FIVE_MINUTES = 600;
        const int TEN_MINUTES = 600;
        const int FIFTEEN_SECONDS = 15;
        const int FIVE_SECONDS = 5;

        public IEnumerable<AbilityInfo> GetAllAbilities()
        {
            var adios           = new SingleAbility(0, "adios"          , "Triggers the homeward bone effect instantly");
            var flashbang       = new SingleAbility(1, "flashbang"      , "Creates a flash of light that fades out over time");
            var poison          = new SingleAbility(2, "poison"         , "Applies the poison debuff");
            var toxic           = new SingleAbility(3, "toxic"          , "Applies the toxic debuff");
            var maggots         = new SingleAbility(4, "maggots"        , "Applies the maggots debuff");
            var heartAttack     = new SingleAbility(5, "heartAttack"    , "Causes instant death");
            var fourteenIsFine  = new SingleAbility(6, "fourteenIsFine" , "Reduces vigor to 14 permanently. Also reduces soul level accordingly");
            var smallBiceps     = new SingleAbility(21, "smallBiceps"   , "Reduces strength by 5. Reduces soul level accordingly");
            var twoLeftHands    = new SingleAbility(22, "twoLeftHands"  , "Reduces dex by 5. Reduces soul level accordingly");

            var exhaust         = new TimedAbility(7, "exhaust"         , "Reduces stamina to 1 for 5 minutes",                                                     FIVE_MINUTES);
            var fullImmersion   = new TimedAbility(8, "fullImmersion"   , "Embrace your ego for 10 minutes. Switches camera to ego perspective",                    TEN_MINUTES);
            var topDown         = new TimedAbility(9, "topDown"         , "Switches the camera to a fixed top down position for 10 minutes",                        TEN_MINUTES);
            var upsideDown      = new TimedAbility(10, "upsideDown"     , "Flips the game upside down for 5 minutes. Left is also right and up is down. Good luck", FIVE_MINUTES);
            var suddenDeath     = new TimedAbility(11, "suddenDeath"    , "Reduces the max hp to 1 for 5 minutes",                                                  FIVE_MINUTES);
            var twiceTheFun     = new TimedAbility(12, "twiceTheFun"    , "All enemies deal twice the damage for 5 minutes",                                        FIVE_MINUTES);
            var speedSouls      = new TimedAbility(13, "speedSouls"     , "Randomizes the global game speed for 5 minutes",                                         FIVE_MINUTES);
            var crackSouls      = new TimedAbility(14, "crackSouls"     , "Sets the global game speed to 1.75 for 5 minutes",                                       FIVE_MINUTES);
            var veryDarkSouls   = new TimedAbility(15, "veryDarkSouls"  , "Dimms the light a bit for 5 minutes",                                                    FIVE_MINUTES);
            var doubleTrouble   = new TimedAbility(16, "doubleTrouble"  , "All enemies have twice as much HP for 5 minutes",                                        FIVE_MINUTES);
            var whipIt          = new TimedAbility(17, "whipIt"         , "Switches the moveset for the right hand to that of the whip",                            FIVE_MINUTES);
            var speedyGonzales  = new TimedAbility(18, "speedyGonzales" , "Grants you twice the speed at the cost of half of your height. Only lasts 15 seconds",   FIFTEEN_SECONDS);
            var yhorm           = new TimedAbility(19, "yhorm"          , "Grants you twice the size at the cost of half of your speed. Only lasts 15 seconds",     FIFTEEN_SECONDS);

            var einstein = new TimedIterativeAbility(20, "einstein", "Grants access to the truth of the universe over time - the brain will adjust", 300,           FIVE_SECONDS);

            return new List<AbilityInfo>() 
            {
                new AbilityInfo(flashbang,      3, Rarity.COMMON),
                new AbilityInfo(poison,         1, Rarity.COMMON),
                new AbilityInfo(maggots,        1, Rarity.COMMON),
                new AbilityInfo(fourteenIsFine, 1, Rarity.COMMON),
                new AbilityInfo(smallBiceps,    1, Rarity.RARE),
                new AbilityInfo(twoLeftHands,   1, Rarity.RARE),
                new AbilityInfo(adios,          1, Rarity.RARE),
                new AbilityInfo(toxic,          1, Rarity.RARE),
                new AbilityInfo(heartAttack,    1, Rarity.VERY_RARE),

                new AbilityInfo(exhaust,        2, Rarity.COMMON),
                //new AbilityInfo(speedSouls,     1, Rarity.COMMON),
                new AbilityInfo(twiceTheFun,    1, Rarity.COMMON),
                new AbilityInfo(suddenDeath,    1, Rarity.COMMON),
                //new AbilityInfo(speedyGonzales, 3, Rarity.COMMON),
                //new AbilityInfo(yhorm,          3, Rarity.COMMON),
                new AbilityInfo(doubleTrouble,  1, Rarity.COMMON),
                new AbilityInfo(fullImmersion,  1, Rarity.RARE),
                //new AbilityInfo(topDown,        1, Rarity.RARE),
                new AbilityInfo(crackSouls,     1, Rarity.RARE),
                new AbilityInfo(veryDarkSouls,  1, Rarity.RARE),
                new AbilityInfo(whipIt,         1, Rarity.RARE),
                //new AbilityInfo(upsideDown,     1, Rarity.VERY_RARE),

                new AbilityInfo(einstein,       1, Rarity.RARE),
            };
        }
    }
}
