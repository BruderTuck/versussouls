﻿using System;
using System.Collections.Generic;
using System.Text;
using VersusLib.interfaces;

namespace VersusLib.abilities.implementations
{
    public class SingleAbility : AbilityBase
    {
        public SingleAbility() : base() { }

        public SingleAbility(int id, string name, string description) : base(id, name, description) { }

        public override IAbility CreateCopy() => new SingleAbility(this.Id, this.Name, this.Description);
    }
}
