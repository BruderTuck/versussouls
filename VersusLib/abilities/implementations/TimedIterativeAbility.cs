﻿using System;
using System.Collections.Generic;
using System.Text;
using VersusLib.interfaces;

namespace VersusLib.abilities.implementations
{
    public class TimedIterativeAbility : IterativeAbility
    {
        public TimedIterativeAbility() : base() { }

        public TimedIterativeAbility(int id, string name, string description, int iterations, int delayBetweenIterationsInSeconds) : base(id, name, description, iterations, delayBetweenIterationsInSeconds) { }

        public override IAbility CreateCopy() => new TimedIterativeAbility(this.Id, this.Name, this.Description, this.Iterations, this.DelayBetweenIterationsInSeconds);
    }
}
