﻿using System;
using System.Collections.Generic;
using System.Text;
using VersusLib.interfaces;

namespace VersusLib.abilities.implementations
{
    public abstract class AbilityBase : IAbility
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Single { get; set; }
        public string Description { get; set; }

        public AbilityBase() { }

        public AbilityBase(int id, string name, string description)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
        }

        public abstract IAbility CreateCopy();
    }
}
