﻿using System;
using System.Collections.Generic;
using System.Text;
using VersusLib.interfaces;

namespace VersusLib.abilities.implementations
{
    public class IterativeAbility : AbilityBase
    {
        public int Iterations { get; set; }
        public int DelayBetweenIterationsInSeconds { get; set; }

        public IterativeAbility() : base() { }

        public IterativeAbility(int id, string name, string description, int iterations, int delayBetweenIterationsInSeconds) : base(id, name, description)
        {
            this.Iterations = iterations;
            this.DelayBetweenIterationsInSeconds = delayBetweenIterationsInSeconds;
        }

        public override IAbility CreateCopy() => new IterativeAbility(this.Id, this.Name, this.Description, this.Iterations, this.DelayBetweenIterationsInSeconds);
    }
}
