﻿using System;
using System.Collections.Generic;
using System.Text;
using VersusLib.interfaces;

namespace VersusLib.abilities.implementations
{
    public class TimedAbility : AbilityBase
    {
        public int DurationInSeconds { get; set; }

        public TimedAbility() : base() { }

        public TimedAbility(int id, string name, string description, int durationInSeconds) : base(id, name, description)
        {
            this.DurationInSeconds = durationInSeconds;
        }

        public override IAbility CreateCopy() => new TimedAbility(this.Id, this.Name, this.Description, this.DurationInSeconds);
    }
}
