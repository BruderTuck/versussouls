﻿using System;
using System.Collections.Generic;
using System.Linq;
using VersusLib.common;
using VersusLib.interfaces;

namespace VersusLib.abilities
{
    public class AbilityFactory : IAbilityFactory
    {
        public IAbilityProvider AbilityProvider { get; set; }
        private readonly IEnumerable<AbilityInfo> _abilities;

        public AbilityFactory(IAbilityProvider abilityProvider)
        {
            this.AbilityProvider = abilityProvider;
            _abilities = this.AbilityProvider.GetAllAbilities();
        }

        public AbilityInfo GetAbilityInfo(int id)
        {
            return _abilities.FirstOrDefault(info => info.Ability.Id == id);
        }

        public AbilityInfo GetWeightedRandomAbilityInfo()
        {
            var commonAbilities = _abilities.Where(ability => ability.Rarity == Rarity.COMMON);
            var rareAbilities = _abilities.Where(ability => ability.Rarity == Rarity.RARE);
            var veryRareAbilities = _abilities.Where(ability => ability.Rarity == Rarity.VERY_RARE);

            var commonPropabilityThreshold = 0.5f;
            var rarePropabilityThreshold = 0.8f;
            var veryRarepropabilityThreshold = 1.0f;

            var randomNumber = new Random().NextDouble();

            if (randomNumber >= 0 && randomNumber < commonPropabilityThreshold)
                return GetRandomAbilityInfo(commonAbilities);
            else if (randomNumber >= commonPropabilityThreshold && randomNumber < rarePropabilityThreshold)
                return GetRandomAbilityInfo(rareAbilities);

            return GetRandomAbilityInfo(veryRareAbilities);
        }

        private AbilityInfo GetRandomAbilityInfo(IEnumerable<AbilityInfo> infos)
        {
            return infos.ToArray()[new Random().Next(0, infos.Count())];
        }

        public IEnumerable<AbilityInfo> GetAllAbilityInfos()
        {
            return _abilities;
        } 
    }
}
