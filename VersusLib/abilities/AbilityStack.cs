﻿using VersusLib.interfaces;

namespace VersusLib.abilities
{
    public class AbilityStack
    {
        public IAbility Ability { get; set; }
        public int Amount { get; set; }
    }
}
