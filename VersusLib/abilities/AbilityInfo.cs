﻿using System;
using System.Collections.Generic;
using System.Text;
using VersusLib.common;
using VersusLib.interfaces;

namespace VersusLib.abilities
{
    public class AbilityInfo
    {
        public IAbility Ability { get; set; }
        public int Quantity { get; set; }
        public Rarity Rarity { get; set; }

        public AbilityInfo()
        {

        }

        public AbilityInfo(IAbility ability, int quantity, Rarity rarity)
        {
            this.Ability = ability;
            this.Quantity = quantity;
            this.Rarity = rarity;
        }
    }
}
