﻿using Nancy.Hosting.Self;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using VersusLib.abilities;
using VersusLib.common;
using VersusLib.interfaces;
using VersusLib.networking;
using VersusLib.user;

namespace VersusLib
{
    public class Runtime
    {
        private static Runtime _instance;
        private IUserStore _userStore;
        private IAbilityFactory _abilityFactory;
        private string FullAPIServerIp = null;
        private Process _trainerProcess = null;
        private Runtime() { }

        public static Runtime GetInstance()
        {
            if (_instance is null)
                _instance = new Runtime();

            return _instance;
        }

        public bool Initialize()
        {
            _userStore = new FileUserStore();
            _userStore.Initialize();
            _abilityFactory = new AbilityFactory(new HardcodedAbilityProvider());
            
            var pathToCurrentFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            FileHandler.SetResourceFolderPath(pathToCurrentFolder);
            
            return true;
        }

        public bool Initialize(NetworkConfig networkConfig)
        {
            this.Initialize();
            NetworkHandler.GetInstance().Initialize(networkConfig);
            _userStore.Initialize();

            var ip = networkConfig.APIServerIp is null ? NetworkHandler.GetLocalIp() : networkConfig.APIServerIp;
            var port = networkConfig.APIServerPort < 0 ? 8080 : networkConfig.APIServerPort;

            this.FullAPIServerIp = $"http://{ip}:{port}";

            return true;
        }

        public void StartAPIServer()
        {
            new Thread(() =>
            {
                Console.WriteLine("Starting Server..");
                var urlReservations = new UrlReservations { CreateAutomatically = true };
                var hostConfig = new HostConfiguration { UrlReservations = urlReservations };

                using var host = new NancyHost(new Uri(FullAPIServerIp), new CustomNancyBootstrapper(), hostConfig);
                host.Start();
                Console.WriteLine($"Running on - {FullAPIServerIp}");
                Console.ReadKey();
            }).Start();
        }

        public void StartClient()
        {
            new Thread(() =>
            {
                Console.WriteLine("Starting Receiver..");
                NetworkHandler.ExecuteReceiver();
                Console.WriteLine("Receiver closed");
            }).Start();

            new Thread(() =>
            {
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    FileName = "VersusSouls.exe",
                    WindowStyle = ProcessWindowStyle.Hidden,
                };

                try
                {
                    Console.WriteLine("Starting trainer...");
                    using Process exeProcess = Process.Start(startInfo);
                    _trainerProcess = exeProcess; 
                    exeProcess.WaitForExit();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error while starting trainer: " + e.StackTrace);
                }
            }).Start();
        }

        public bool Shutdown()
        {
            if(_trainerProcess != null)
                _trainerProcess.Close();
            return true;
        }

        public IUserStore GetUserStore() => _userStore;

        public IAbilityFactory GetAbilityFactory() => _abilityFactory;
    }
}
