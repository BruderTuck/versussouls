﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VersusLib.common
{
    public class Constants
    {
        public const string CONTENT_FOLDER_NAME = "_content";
        public const int DEFAULT_COMMAND_SOCKET_PORT = 8081;
        public const int DEFAULT_API_SERVER_PORT = 8080;
    }
}
