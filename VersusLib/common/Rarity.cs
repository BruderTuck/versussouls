﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VersusLib.common
{
    public enum Rarity
    {
        COMMON,
        RARE,
        VERY_RARE
    }
}
