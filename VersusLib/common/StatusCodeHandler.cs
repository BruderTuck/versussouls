﻿using Nancy;
using Nancy.ErrorHandling;
using Nancy.Responses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace VersusLib.common
{
    public class StatusCodeHandler : IStatusCodeHandler
    {
        public void Handle(HttpStatusCode statusCode, NancyContext context)
        {
            var path = Path.Combine("_content", "index.html");
            var response = new GenericFileResponse(path, context);
            context.Response = response;
        }

        public bool HandlesStatusCode(HttpStatusCode statusCode, NancyContext context)
        {
            return statusCode == HttpStatusCode.NotFound;
        }
    }
}
