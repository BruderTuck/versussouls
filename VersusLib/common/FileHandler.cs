﻿using System;
using System.IO;
using System.Text.Json;

namespace VersusLib.common
{
    public static class FileHandler
    {
        private const string COMMAND_PIPE_FILE_NAME = "versus_command_pipe.txt";
        private const string USER_SAVE_FILE_NAME = "versus_user.txt";

        private static string _pathToUserSaveFile;
        private static string _pathToCommandPipeFile;
        private static string _pathToContentFolder;

        public static void SetResourceFolderPath(string pathToResourceFolder)
        {
            _pathToUserSaveFile = Path.Combine(pathToResourceFolder, USER_SAVE_FILE_NAME);
            _pathToCommandPipeFile = Path.Combine(pathToResourceFolder, COMMAND_PIPE_FILE_NAME);
            _pathToContentFolder = Path.Combine(pathToResourceFolder, Constants.CONTENT_FOLDER_NAME);
        }
        public static void WriteToFile(string pathToFile, string text, bool append)
        {
            using var file = new StreamWriter(pathToFile, append);
            file.WriteLine(text);
        }

        public static string ReadFile(string pathToFile)
        {
            if (File.Exists(pathToFile))
            {
                using var file = new StreamReader(pathToFile);
                var contents = file.ReadToEnd();
                return contents;
            }
            return null;
        }

        public static void AppendCommandPipeFile(string text)
        {
            WriteToFile(_pathToCommandPipeFile, text, true);
        }

        public static void WriteUserSave(string text)
        {
            WriteToFile(_pathToUserSaveFile, text, false);
        }

        public static string ReadUserSaveFile()
        {
            return ReadFile(_pathToUserSaveFile);
        }

        public static string GetContentFile(string path)
        {
            var fullPath = Path.Combine(_pathToContentFolder, path);
            return ReadFile(fullPath);
        }
    }
}
