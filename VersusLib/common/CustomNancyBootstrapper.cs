﻿using Nancy;
using Nancy.Conventions;
using System;
using System.Collections.Generic;
using System.Text;

namespace VersusLib.common
{
    public class CustomNancyBootstrapper : DefaultNancyBootstrapper
    {

        protected override void ConfigureConventions(NancyConventions conventions)
        {
            base.ConfigureConventions(conventions);

            conventions.StaticContentsConventions.Add(
                StaticContentConventionBuilder.AddDirectory("/", Constants.CONTENT_FOLDER_NAME)
            );
        }
    }
}
